/// <reference types="cypress" />
/**
 * playwright.js
 * https://medium.com/tech-learn-share/shall-we-integrate-cypress-io-with-safari-browser-c190d600e771
 *
 * Ajouter dans plugin/index.js
 * const { playwright } = require('./cypress/support/playwright')
 *
 * Ajouter dans plugin/index.js
 * on('task', {
 *  async openPlaywright({ browser, platform, idAssure }) {
 *      return await playwright(browser, platform, idAssure)
 *  }
 * })
 *
 */
const { chromium, firefox, webkit } = require("playwright");
// Set the env variable.
process.env.DEBUG = "pw:api,pw:browser*";

let browser, context, page;
const options = {
  headless: false,
  slowMo: 250,
};
/**
 * Playwright launcher
 * @param {*} browser : browser name
 * @param {*} platform : platform name (temp-phone-number, quackr)
 * @param {*} idAssure : id de l'assuré
 * @returns
 */
exports.playwright = async function playwright(b) {
  if (b == "chrome") browser = await chromium.launch(options);
  if (b == "firefox") browser = await firefox.launch(options);
  if (b == "webkit") browser = await webkit.launch(options);

  context = await browser.newContext();
  page = await context.newPage();
  await page
    .goto("https://www.younup.fr", { waitUntil: "load", timeout: 5000 })
    .then(() => {
      console.log("success");
    })
    .catch((res) => {
      console.log("fails", res);
    });
  await page.click("text=Blog", { timeout: 5000 });
  await browser.close();
  return null;
};
