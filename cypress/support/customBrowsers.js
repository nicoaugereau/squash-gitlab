/// <reference types="cypress" />
/**
 * Cypress customize browsers
 * https://docs.cypress.io/guides/guides/launching-browsers#Customize-available-browsers
 *
 * Browsers à déposer dans le répertoire racine du projet sesame-tests : ./browsers
 */
const execa = require('execa')
const path = require('path')
const fs = require('fs')
const { platform } = require('os'),
    osType = platform()
let browserPath, newBrowser, fullVersion, majorVersion
let browsersList = (customList = winList = macList = linuxList = [])

/**
 * Find config file in the ./browsers folder
 */
const findConfigFile = () => {
    browserPath = path.join(__dirname, '../..', 'browsers', 'config.json')

    if (fs.existsSync(browserPath)) {
        customList = require(browserPath)
        return customList
    } else {
        return false
    }
}

/**
 * Find Windows browsers in the ./browsers folder
 * Add <fullVersion>.manifest in the browser folder
 */
const winBrowsers = () => {
    browserPath = path.join(__dirname, '../..', 'browsers')

    const directoriesInDIrectory = fs
        .readdirSync(browserPath, { withFileTypes: true })
        .filter(item => item.isDirectory())
        .map(item => item.name)

    for (const element of directoriesInDIrectory) {
        let dirCont = fs.readdirSync(`${browserPath}/${element}`)
        let files = dirCont.filter(function (elm) {
            //return elm.match(/.*\.manifest/gi)
            return elm.match(/^(\d+\.)?(\d+\.)?(\*|\d+.)?(\*|\d+)/gi)
        })
        if (files.length > 0) {
            ;[, fullVersion] = /(\d+\.\d+\.\d+\.\d+)/.exec(files)
            majorVersion = parseInt(fullVersion.split('.')[0])
        } else {
            fullVersion = '74.0.0.0'
            majorVersion = 74
        }

        exeName = element.toLowerCase() == 'chromium' ? 'chrome' : element
        newBrowser = {
            name: element.toLowerCase(),
            family: 'chromium',
            channel: 'stable',
            displayName: element,
            version: fullVersion,
            path: `${browserPath}\\${element}\\${exeName}.exe`,
            majorVersion: majorVersion
        }
        winList = winList.concat(newBrowser)
    }
    return winList
}

/**
 * Find MacOS browsers in the /Applications folder
 */
const macBrowsers = () => {
    // the path is hard-coded for simplicity
    //const browserPath = '/Applications/Brave Browser.app/Contents/MacOS/Brave Browser'
    macList = ['Brave Browser', 'Opera'] // Browsers not in the Cypress default list

    // read filesystem to find browsers in macList and execute --version command
    for (const element of macList) {
        browserPath = `/Applications/${element}.app/Contents/MacOS/${element}`
        execa(browserPath, ['--version']).then(result => {
            // STDOUT will be like "Brave Browser 77.0.69.135"
            ;[, fullVersion] = /Brave Browser (\d+\.\d+\.\d+\.\d+)/.exec(result.stdout)
            majorVersion = parseInt(fullVersion.split('.')[0])

            newBrowser = {
                name: element,
                family: 'chromium',
                channel: 'stable',
                displayName: element,
                version: fullVersion,
                path: browserPath,
                majorVersion: majorVersion
            }
            macList = macList.concat(newBrowser)
        })
    }
}

/**
 * Find Linux browsers in the /?? folder
 */
const linuxBrowsers = () => {
    linuxList = ['brave-browser', 'opera'] // Browsers not in the Cypress default list

    function capitalizeFirstLetter(words) {
        var separateWord = words.toLowerCase().split('-')
        for (var i = 0; i < separateWord.length; i++) {
            separateWord[i] = separateWord[i].charAt(0).toUpperCase() + separateWord[i].substring(1)
        }
        return separateWord.join(' ')
    }

    // read filesystem to find browsers in linuxList and execute --version command
    for (const element of macList) {
        browserPath = `/usr/bin/${element}`
        execa(browserPath, ['--version']).then(result => {
            // STDOUT will be like "Brave Browser 77.0.69.135"
            ;[, fullVersion] = /Brave Browser (\d+\.\d+\.\d+\.\d+)/.exec(result.stdout)
            majorVersion = parseInt(fullVersion.split('.')[0])

            newBrowser = {
                name: capitalizeFirstLetter(element),
                family: 'chromium',
                channel: 'stable',
                displayName: element,
                version: fullVersion,
                path: browserPath,
                majorVersion: majorVersion
            }
            linuxList = linuxList.concat(newBrowser)
        })
    }
}

function findBrowsers(config) {
    browserPath = path.join(__dirname, '../..', 'browsers')

    // browsers folder exists ?
    if (fs.existsSync(browserPath)) {
        // config.json exists ?
        findConfigFile()
        // which OS ?
        if (osType == 'win32') return (config.browsers = config.browsers.concat(customList, winBrowsers()))
        if (osType == 'darwin') {
            //browsersList = macList.concat(customList, macBrowsers())
            //return (config.browsers = config.browsers.concat(customList, macBrowsers()))
        }
        if (osType == 'linux') {
            //browsersList = linuxList.concat(customList, linuxBrowsers())
            // return (config.browsers = config.browsers.concat(customList, linuxBrowsers()))
        }
        //return browsersList
    } else {
        return false
    }
}

module.exports = findBrowsers
