![Cypress](https://img.shields.io/badge/cypress-12.3.0-green.svg)
![Cucumber Preprocessor](https://img.shields.io/badge/cypress--cucumber--preprocessor-15.1.2-green.svg)

# Squash Gitlab project

This project is designed to run with Squath TM, QA application from Henix [squashtest.com](https://www.squashtest.com/)

📣 This project has been updated to Cypress v12 (with cypress-cucumber-preprocessor v15.1.2).

📣 Updates are coming soon to detail the operation and configuration.

<br>

## Starting this project on your machine

<br>

### Clone the repository

```
git clone https://gitlab.com/nicoaugereau/squash-gitlab.git
```

### Go to the directory

```
cd <nome-da-pasta>
```

### Install the dependencies

```
npm install
```

### Cypress tests

Run cypress tests (open mode or run mode)

```
npm run cypress:open
npm run cypress:run
```

# About

[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/nicolasaugereau/)
